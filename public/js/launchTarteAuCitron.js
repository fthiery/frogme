
tarteaucitron.user.matomoId = 3;
(tarteaucitron.job = tarteaucitron.job || []).push('matomo');

tarteaucitron.user.matomoHost = '//matomo.galar.ovh/';
var _paq = _paq || [];
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function () {
    var u = "//matomo.galar.ovh/";
    _paq.push(['setTrackerUrl', u + 'matomo.php']);
    _paq.push(['setSiteId', 3]);
    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript'; g.async = true; g.defer = true; g.src = u + 'matomo.js'; s.parentNode.insertBefore(g, s);
})();

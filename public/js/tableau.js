$(document).ready(function () {
    Papa.parse('data/listesolutions.csv', {
        download: true,
        header: true,
        skipEmptyLines: true,
        complete: function (results) {
            var table = $('#myTable').DataTable({
                data: results.data,
                pageLength: 25,
                columns: [
                    {
                        data: null,
                        render: function (data, type, row) {
                            var familles = [
                                row['Zone'], row['Quartier'], row['Ilot']
                            ].filter(Boolean).join(' <strong>/</strong> ');
                            return familles;
                        }
                    },
                    {
                        data: 'Nom',
                        render: function (data, type, row) {
                            if (type === 'display') {
                                var url = row['URL'];
                                return '<a href="' + url + '" target="_blank">' + data + '</a>';
                            }
                            return data;
                        }
                    },
                    {
                        data: 'Description'
                    },
                    {
                        data: 'Type'
                    }, {
                        data: null,
                        render: function (data, type, row) {
                            var certifications = ['Secnumcloud', 'HDS', '27001', '27701','27018:2019', '27017:2015', '20000-1:2018', '50001', 'HDA/HADS', 'SOC 2 Type II', 'SOC 1 Type II', '14001', 'PCI-DSS' , '14040', 'SecNumCloud en cours','SecNumCloud en projet', 'Utilise un hébergeur SecNumCloud', 'Homologation DINUM'].filter(function (column) {
                                var value = row[column];
                                return value && value.trim() !== "?" && value.trim() !== "Non";
                            }).join(' <strong>/</strong> ');
                            return certifications;
                        }
                    }, {
                        data: null,
                        render: function (data, type, row) {
                            var Reseaux = '';
                            if (row['Twitter']) {
                                Reseaux += ' <a href="' + row['Twitter'] + '" target="_blank"><img class="logo-image" src="images/X-logo-black.png" alt="Twitter"></a>';
                            }

                            if (row['Linkedin']) {
                                Reseaux += ' <a href="' + row['Linkedin'] + '" target="_blank"><img class="logo-image" src="images/LI-In-Bug.png" alt="LinkedIn"></a>';
                            }
                            return Reseaux;
                        }
                    }
                ],
                columnDefs: [
                    {
                        width: '10px',
                        targets: [
                            0,
                            1,
                            2,
                            3,
                            4,
                            5
                        ]
                    }
                ],
                responsive: true,
                fixedColumns: {
                    leftColumns: 0,
                    rightColumns: 0
                },
                language: {
                    "url": "https://cdn.datatables.net/plug-ins/1.13.7/i18n/fr-FR.json"
                }
            });
        }
    });
});
